var height = $(window).height();

$(window).scroll(function() {
    if ($("header").offset().top > height) {
        $("header").addClass("active");
    } else {
        $("header").removeClass("active");
    }
});

$(document).ready(function(){
    $('.__elt-burguer').on('click', function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.__elt-nav-items').removeClass('active');
        }else{
            $(this).addClass('active');
            $('.__elt-nav-items').addClass('active');
        }
    })

    $('.__elt-nav-items li a').on('click', function(){
        $('.__elt-nav-items').removeClass('active');
        $('.__elt-burguer').removeClass('active');
    });

    $( ".__elt-nav-items li a" ).click(function( event ) {
		event.preventDefault();
		$("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 74 }, 500);
	});
});