// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
"use strict";

const gulp = require("gulp"),
      sass = require('gulp-sass'),
      uglify = require('gulp-uglify-es').default,
      del = require("del"),
      postcss = require('gulp-postcss'),
      autoprefixer = require('autoprefixer'),
      cssnano = require('cssnano'),
      cleanCSS = require('gulp-clean-css'),
      debug = require('gulp-debug'),
      include = require('gulp-include'),
      gcmq = require('gulp-group-css-media-queries'),
      gutil = require('gulp-util');

// File paths
var paths = {
    
    // files path
    scripts: './app/**/*.js',
    styles: './app/**/*.{scss,sass,css}',
    // dist path
    dist: './dist',

    // watch dist path
    distJs: './dist/arquivos/*.js',
    distCss: './dist/arquivos/*.css',
};

// Gulp Clean

function clean() {
    return del([paths.dist]);
};

// Gulp Css

function css() {
    return gulp
    .src(paths.styles)
    .pipe(sass())
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(gcmq())
    .pipe(gulp.dest('dist/'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/'))
}

// Gulp Js

function scripts() {
    return (
      gulp
      .src(paths.scripts)
      .pipe(debug({
          title: 'files:'
      }))
      .pipe(include().on('error', gutil.log))
      .pipe(gulp.dest('dist/'))
      .pipe(uglify().on('error', gutil.log))
      .pipe(gulp.dest('dist/'))
    );
}

// Gulp Tasks

const js = gulp.series(scripts);
const dist = gulp.series(clean, gulp.parallel(css, js));

exports.css = css;
exports.js = js;
exports.clean = clean;
exports.dist = dist;
exports.default = dist;